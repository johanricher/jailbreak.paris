function withoutTrailingSlash(s) {
	return s ? s.replace(/\/$/, "") : null
}

export const baseUrl = withoutTrailingSlash(process.env.BASE_URL)
if (!baseUrl) {
	throw Error(`Undefined environment variable: BASE_URL`)
}

export const matomoBaseUrl = process.env.MATOMO_BASE_URL ? withoutTrailingSlash(process.env.MATOMO_BASE_URL) : null
export const matomoSiteId = process.env.MATOMO_SITE_ID || null

export const twitterUrl = process.env.TWITTER_URL || null
