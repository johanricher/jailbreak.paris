// Adapted from code copy-pasted from Matomo.
export function matomoTrackingCode(matomoBaseUrl, matomoSiteId) {
	return `
    <script type="text/javascript">
      var _paq = window["_paq"] || []
      /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
      _paq.push(["trackPageView"])
      _paq.push(["enableLinkTracking"])
      ;(function() {
        _paq.push(["setTrackerUrl", "${matomoBaseUrl}/matomo.php"])
        _paq.push(["setSiteId", "${matomoSiteId}"])
        var d = document,
          g = d.createElement("script"),
          s = d.getElementsByTagName("script")[0]
        g.type = "text/javascript"
        g.async = true
        g.defer = true
        g.src = "${matomoBaseUrl}/matomo.js"
        s.parentNode.insertBefore(g, s)
      })()
    </script>
  `
}
